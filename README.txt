
/*************************************************************

HOMEWORK 1- SENTIMENT ANALYSIS (TEXT CATEGORIZATION)
GAURAV PIYUSH (108996990)
CHAITANYA SAXENA (108346525)

*************************************************************/
Prerequisites:
	Eclipse Keplar or higher
	LibLinear
	SVMLight


Running the code:

1. Open a JAVA IDE (preferrably, Eclipse). Import nlp_hw1 as a java project.
2. For language model classifier, run the LanguageModelClassifier.java 
as a JAVA application.
3. For perceptron classifier, run the Perceptron1.java
4(a). For SVM classifier, run the SVMClassifier.java as a JAVA application.
4(b). Now open the command line and open the file svm_cmd_libsvm.txt and 
copy commands  in an orderly manner to run the svm classifier.