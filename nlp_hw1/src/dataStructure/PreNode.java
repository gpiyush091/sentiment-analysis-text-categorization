package dataStructure;

import java.util.HashMap;

public class PreNode {
	public String filename;
	public HashMap<String, Integer> unigramCount = new HashMap<>();
	public HashMap<String, Integer> bigramCount = new HashMap<>();
	public Boolean category;
	public PreNode(String filename, HashMap<String, Integer> u, HashMap<String, Integer> b, Boolean c) {
		this.filename = filename;
		this.unigramCount.putAll(u);
		this.bigramCount.putAll(b);
		this.category = c;
	}
	public void setCategory(boolean cat){
		category=cat;
		
		
	}
}
