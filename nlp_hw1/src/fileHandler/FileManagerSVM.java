package fileHandler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import classifier.Stemmer;
import dataStructure.PreNode;

public class FileManagerSVM {
	/*
	 * collection of all documents 
	 * */
	ArrayList<PreNode> documentCollection = new ArrayList<>();
	String[] stopwords = {"a","able","about","across","after","all","almost","also","am","among","an","and","any","are","as","at","be","because","been","but","by","can","cannot","could","dear","did","do","does","either","else","ever","every","for","from","get","got","had","has","have","he","her","hers","him","his","how","however","i","if","in","into","is","it","its","just","least","let","like","likely","may","me","might","most","must","my","neither","no","nor","not","of","off","often","on","only","or","other","our","own","rather","said","say","says","she","should","since","so","some","than","that","the","their","them","then","there","these","they","this","tis","to","too","twas","us","wants","was","we","were","what","when","where","which","while","who","whom","why","will","with","would","yet","you","your"};
	private int corpusSize = 0;
	
	public int getCorpusSize() {
		return corpusSize;
	}
	
	public ArrayList<PreNode> getCollection() {
		return documentCollection;
	}
	
	public void readAllData (String folderPath, Boolean category) 
	{	
		File folder = new File(folderPath);
		if (folder.isDirectory()) {
			File[] files = folder.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File folder, String fileName) {
					return fileName.endsWith(".txt");
				}
			});
			
			/* Commented as it was used for debugging purposes */
			// String filename = "F:\\remote\\CSE599\\TestData-Rush\\rush-003690.html";

			for (int i=0; i<files.length; i++) {
				// if (filename.equals(files[i].toString())){
					parseText(files[i], category);
					// break;
				// }
			}	
		}
	}
	
	private void parseText(File textfile, Boolean category) {
		String str=null;
		String allText = "";
		BufferedReader br = null;
		
		HashMap<String, Integer> unigramCount = new HashMap<>();
		HashMap<String, Integer> bigramCount = new HashMap<>();
		
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(textfile), "UTF-8"));
			while ((str = br.readLine())!=null){
				
				// Get all unigrams of length greater than 2
				StringTokenizer tokenizer = new StringTokenizer(str);
				
				while (tokenizer.hasMoreElements()) {
					Boolean flagStop = false;
					String element = tokenizer.nextElement().toString().toLowerCase();
					// Check for stop words and stemmer
					for (int j = 0;j<stopwords.length;j++) {
						if (element.equals(stopwords[j])) {
							flagStop = true;
							break;
						}
					}
					if (flagStop) {
						continue;
					}
					
					// Stemmer
					Stemmer stem = new Stemmer();
					stem.add(element.toCharArray(), element.toCharArray().length);
					stem.stem();
					element = stem.toString();
					corpusSize++;
					
					// Put in hash map. if already in hash map increase count.
					if (!unigramCount.containsKey(element)) {
						// new word
						unigramCount.put(element, 1);
					} else {
						// already present word
						unigramCount.put(element, 
								unigramCount.get(element)+1);
					}
				}
				allText = allText.concat(str);
			}
			
			// Get all bigrams
			StringTokenizer tokenizer = new StringTokenizer(allText);
			String prevElemString = null;
			while (tokenizer.hasMoreElements()) {
				String bigramStr;
				if (prevElemString == null) {
					// reading the first string
					prevElemString = tokenizer.nextElement().toString().toLowerCase();
				}
				if (!tokenizer.hasMoreElements()) {
					break;
				}
				
				String secondWord = tokenizer.nextElement().toString().toLowerCase();
				bigramStr = ""+prevElemString+" "+secondWord;
				prevElemString = secondWord;
				
				// Put bigram in appropriate hashmap
				if (bigramCount.get(bigramStr) == null) {
					bigramCount.put(bigramStr, 1);
				} else {
					bigramCount.put(bigramStr, bigramCount.get(bigramStr)+1);
				}
			}
			
			PreNode documentStats = new PreNode(textfile.getName(), unigramCount, bigramCount, category);
			
			documentCollection.add(documentStats);
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br!=null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
