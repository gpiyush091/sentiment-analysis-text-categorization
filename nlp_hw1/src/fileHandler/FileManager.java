package fileHandler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import dataStructure.PreNode;

public class FileManager {
	
	/*
	 * collection of all documents 
	 * */
	ArrayList<PreNode> documentCollection = new ArrayList<>();
	
	private int corpusSize = 0;
	
	public int getCorpusSize() {
		return corpusSize;
	}
	
	public ArrayList<PreNode> getCollection() {
		return documentCollection;
	}
	
	public void readAllData (String folderPath, Boolean category) 
	{	
		File folder = new File(folderPath);
		if (folder.isDirectory()) {
			File[] files = folder.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File folder, String fileName) {
					return fileName.endsWith(".txt");
				}
			});
			
			/* Commented as it was used for debugging purposes */
			// String filename = "F:\\remote\\CSE599\\TestData-Rush\\rush-003690.html";

			for (int i=0; i<files.length; i++) {
				// if (filename.equals(files[i].toString())){
					parseText(files[i], category);
					// break;
				// }
			}	
		}
	}
	
	private void parseText(File textfile, Boolean category) {
		String str=null;
		String allText = "";
		BufferedReader br = null;
		
		HashMap<String, Integer> unigramCount = new HashMap<>();
		HashMap<String, Integer> bigramCount = new HashMap<>();
		
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(textfile), "UTF-8"));
			while ((str = br.readLine())!=null){
				
				// Get all unigrams of length greater than 2
				StringTokenizer tokenizer = new StringTokenizer(str);
				
				while (tokenizer.hasMoreElements()) {
					corpusSize++;
					String element = tokenizer.nextElement().toString().toLowerCase();
					// Put in hash map. if already in hash map increase count.
					if (!unigramCount.containsKey(element)) {
						// new word
						unigramCount.put(element, 1);
					} else {
						// already present word
						unigramCount.put(element, 
								unigramCount.get(element)+1);
					}
				}
				allText = allText.concat(str);
			}
			
			// Get all bigrams
			StringTokenizer tokenizer = new StringTokenizer(allText);
			String prevElemString = null;
			while (tokenizer.hasMoreElements()) {
				String bigramStr;
				if (prevElemString == null) {
					// reading the first string
					prevElemString = tokenizer.nextElement().toString();
				}
				if (!tokenizer.hasMoreElements()) {
					break;
				}
				
				String secondWord = tokenizer.nextElement().toString();
				bigramStr = ""+prevElemString+" "+secondWord;
				prevElemString = secondWord;
				
				// Put bigram in appropriate hashmap
				if (bigramCount.get(bigramStr) == null) {
					bigramCount.put(bigramStr, 1);
				} else {
					bigramCount.put(bigramStr, bigramCount.get(bigramStr)+1);
				}
			}
			PreNode documentStats = new PreNode(textfile.getName(), unigramCount, bigramCount, category);
			
			documentCollection.add(documentStats);
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br!=null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
