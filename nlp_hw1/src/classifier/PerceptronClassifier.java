package classifier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dataStructure.PreNode;
import fileHandler.FileManager;

public class PerceptronClassifier {
	public double learnRate= 0.1;
	//public int weights;

	ArrayList<Integer> weights;
	
	boolean[] testCategory;



	//public PerceptronClassifier(Document){}
	public void train(String posDir,String negDir){

		//int corpusSize;
		ArrayList<PreNode> docCollection;docCollection=new ArrayList<PreNode>();
		int iterations=0;
		int tempWeight=0;
		int tempUnigramFreq=0;
		int multiplication=0;
		int unigramSizeInDoc=0;
		int weightIteration=0;
		int totalUnigramCountForAllDocuments=0;
		FileManager fm = new FileManager();


		//String posP = "F:\\remote\\Spring'14\\NLP\\HW1\\repo\\train\\train1\\pos";
		//String negP = "F:\\remote\\Spring'14\\NLP\\HW1\\repo\\train\\train1\\neg";
		fm.readAllData(posDir,true);
		fm.readAllData(negDir,false);
		docCollection=fm.getCollection();
		//=fm.getCorpusSize();

		// iterating throught the list of 800 documents(each document is represented by a prenode)..so
		// 1600 prenodes in the docCollection !! 800 prenodes +ve and 800 -ve

		//calculate the total number of unigrams in all the documents and initialize the weight vector with 
		//the count.
		for(PreNode p:docCollection){
			
			totalUnigramCountForAllDocuments+=p.unigramCount.size();
		}
		weights = new ArrayList<Integer>(Collections.nCopies(totalUnigramCountForAllDocuments,0));
		// Perform around 40 iterations !!!

		for(;iterations<1;iterations++){


			//for each document(prenode) in the total 800 documents, do -->>>> 
			for(PreNode p:docCollection){
				unigramSizeInDoc=p.unigramCount.size();

				tempWeight=0;
				tempUnigramFreq=0;
				multiplication=0;
				ArrayList<String> unigrams = new ArrayList<String>(p.unigramCount.keySet());
				//trainCategory.add(p.unigramCount.keySet());
				// iterate through a document and perform the training !! 
				for(int j=0;j<unigramSizeInDoc;j++){


					tempWeight=weights.get(weightIteration+j);

					tempUnigramFreq=p.unigramCount.get(unigrams.get(j));
					multiplication+=tempWeight*tempUnigramFreq;

				}
				if(multiplication==0){  //by default consider it positive !! w=w+f
					for (int j=0;j<unigramSizeInDoc;j++){
						tempWeight=weights.get(weightIteration+j);
						tempUnigramFreq=p.unigramCount.get(unigrams.get(j));
						tempWeight+=tempUnigramFreq;
						weights.add(weightIteration+j,tempWeight);
					}

				}
				else if(multiplication>0){  //considering it positive ! w=w+f 
					if(!p.category){ //but if actual document review is negative then w=w-f
						for (int j=0;j<unigramSizeInDoc;j++){
							tempWeight=weights.get(weightIteration+j);
							tempUnigramFreq=p.unigramCount.get(unigrams.get(j));
							tempWeight-=tempUnigramFreq;
							weights.add(weightIteration+j,tempWeight);
						}

					}
				}
				else{   //considering it negative ! w=w-f
					if(p.category){  // but if actual document review is positive then w=w+f
						for (int j=0;j<unigramSizeInDoc;j++){
							tempWeight=weights.get(weightIteration+j);
							tempUnigramFreq=p.unigramCount.get(unigrams.get(j));
							tempWeight+=tempUnigramFreq;
							weights.add(weightIteration+j,tempWeight);
						}

					}

				}


				//before going to the next document,update the index of the weight arraylist.
				weightIteration+=unigramSizeInDoc;
			}



		}


	}
	public void classify(String posPath,String negPath){
		ArrayList<PreNode> docCollection1=new ArrayList<PreNode>();
		testCategory=new boolean[docCollection1.size()];

		FileManager fm1 = new FileManager();

		fm1.readAllData(posPath,null);
		fm1.readAllData(negPath,null);
		docCollection1=fm1.getCollection();
		int unigramSizeInDoc=0;
		int tempWeight=0;
		int tempUnigramFreq=0;
		int multiplication=0;
		int weightIteration=0;
		int docIteration=0;
	

		for(PreNode p:docCollection1){
			//testCategory[docIteration]=p.category;
			unigramSizeInDoc=p.unigramCount.size();
			tempWeight=0;
			tempUnigramFreq=0;
			multiplication=0;
			ArrayList<String> unigrams = new ArrayList<String>(p.unigramCount.keySet());

			// iterate through a document and perform the training !! 
			for(int j=0;j<unigramSizeInDoc;j++){
				tempWeight=weights.get(weightIteration+j);
				tempUnigramFreq=p.unigramCount.get(unigrams.get(j));
				multiplication+=tempWeight*tempUnigramFreq;

			}

			if(multiplication>0){  //considering it positive ! w=w+f 
				p.setCategory(true);
			}
			else{   //considering it negative ! w=w-f
				p.setCategory(false);
			}
			//before going to the next document,update the index of the weight arraylist.
			weightIteration+=unigramSizeInDoc;
			testCategory[docIteration]=p.category;
			docIteration++;
			
			/*if(flag){

				System.out.println("Document result "+p.category);	

			} */
		}


	}
	
	public float getAccuracy(){
		int count=0;
		for (int i=0;i<testCategory.length;i++){
			if(testCategory[i]==true){
			count++;	
			}
		}
		
		
		return ((float)count/testCategory.length)*100;
	}

	public static void main(String args[]){
		float accuracy;
		PerceptronClassifier pc=new PerceptronClassifier();	
		pc.train("/Users/chaitanya/Documents/workspace/nlp_hw1/train/train1/pos","/Users/chaitanya/Documents/workspace/nlp_hw1/train/train1/pos");	
		pc.classify("/Users/chaitanya/Documents/workspace/nlp_hw1/test/test1/pos","/Users/chaitanya/Documents/workspace/nlp_hw1/test/test1/neg");
		accuracy=pc.getAccuracy();
		System.out.println("The accuracy is "+accuracy);
	}
}


