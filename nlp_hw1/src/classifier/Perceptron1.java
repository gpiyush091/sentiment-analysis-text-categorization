package classifier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fileHandler.FileManager;

public class Perceptron1 {

	float err = 0.0f;
	float threshold=  0.5f;
	float bias = 0.0f;
	float alpha = 0.1f; // Learning rate
	
	class resNode {
		String classifierResult;
		String actualResult;
	}
	
	HashMap<String, Float> weight = new HashMap<>();
	HashMap<String, resNode> resultSet = new HashMap<>();
	
	public ArrayList<Float> allAccur = new ArrayList<>();
	
	public static void main(String[] args) {
		System.out.println("Game Begins");
		String dir = "F:\\remote\\Spring'14\\NLP\\HW1\\repo\\";
		Perceptron1 p = new Perceptron1();
		p.segragrateData(dir+"train\\train1\\pos", dir+"train\\train1\\neg", dir+"test\\test1\\pos", dir+"test\\test1\\neg");
		p.segragrateData(dir+"train\\train2\\pos", dir+"train\\train2\\neg", dir+"test\\test2\\pos", dir+"test\\test2\\neg");
		p.segragrateData(dir+"train\\train3\\pos", dir+"train\\train3\\neg", dir+"test\\test3\\pos", dir+"test\\test3\\neg");
		p.segragrateData(dir+"train\\train4\\pos", dir+"train\\train4\\neg", dir+"test\\test4\\pos", dir+"test\\test4\\neg");
		p.segragrateData(dir+"train\\train5\\pos", dir+"train\\train5\\neg", dir+"test\\test5\\pos", dir+"test\\test5\\neg");
		p.accuracyLevels();
		System.out.println("Game Ends");
	}
	
	public void segragrateData(String trainPos, String trainNeg, String testPos, String testNeg) {
		FileManager fmPos = new FileManager();
		FileManager fmNeg = new FileManager();
		
		fmPos.readAllData(trainPos, true);
		fmNeg.readAllData(trainNeg, false);
		
		PreprocessData pd = new PreprocessData();
		pd.processDataCollection(fmPos, fmNeg);
		
		FileManager fm = new FileManager();
		
		fm.readAllData(trainPos, true);
		fm.readAllData(trainNeg, false);
		
		trainSet(fm, pd);
		
		FileManager testSet1 = new FileManager(); // pos
		testSet1.readAllData(testPos, null);
		test(testSet1, true);
	
		FileManager testSet2 = new FileManager(); // neg
		testSet2.readAllData(testNeg, null);
		test(testSet2, false);
		
		// displayHashMap();
		computeAccuracy();
		resultSet.clear();
	}
	
	public void accuracyLevels() {
		float sum = 0.0f;
		for (int i=0;i<allAccur.size();i++) {
			System.out.println("iteration: "+i+" accuracy: "+allAccur.get(i));
			sum += allAccur.get(i);
		}
		System.out.println("Avg accuracy: "+sum/5);
	}
	
	private void computeAccuracy() {
		int accCount = 0;
		int netCount = resultSet.size();
		Iterator it = resultSet.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, resNode> pair = (Map.Entry<String, resNode>) it.next();
			if (pair.getValue().classifierResult.equals(pair.getValue().actualResult)) {
				accCount++;
			} else {
				// System.out.println("Incorrect ones:"+pair.getKey());
			}
		}
		System.out.println("accCount: "+accCount+" netCount: "+netCount);
		float percentage = ((float)accCount/netCount)*100;
		System.out.println("Accuracy: "+ percentage);
		allAccur.add(percentage);
	}
	
	private void test(FileManager fm, Boolean category) {
		for (int i=0;i<fm.getCollection().size();i++) {
			List<Integer> unigramCountList = new ArrayList<>(fm.getCollection().get(i).unigramCount.values());
			int max = Collections.max(unigramCountList);
			Iterator it = fm.getCollection().get(i).unigramCount.entrySet().iterator();
			float sum=0.0f;
			while (it.hasNext()) {
				Map.Entry<String, Integer> pair = (Entry<String, Integer>) it.next();
				if (weight.containsKey(pair.getKey())) {
					sum+=weight.get(pair.getKey())*(((float)pair.getValue())/max);
				}
			}
			
			// System.out.println(sum);
			resNode node = new resNode();
			if (category)
				node.actualResult = "positive"; 
			else 
				node.actualResult = "negative";
			
			if (sum>0)
				node.classifierResult = "positive";
			else 
				node.classifierResult = "negative";
			resultSet.put(fm.getCollection().get(i).filename, node);
		}
	}
	
	private void trainSet(FileManager fm, PreprocessData pd) {
		// Initialize weightVectors to 0
		List<String> unigramsList = new ArrayList<>(pd.unigramCollection.keySet());
		
		for (int i=0;i<unigramsList.size();i++)
			weight.put(unigramsList.get(i), 0.0f);
		
		// Desired output is 1(positive) or 0(negative)
		
		for (int iteration = 0; iteration<600; iteration++ ) {
			for (int i=0;i<fm.getCollection().size();i++) {
				List<Integer> unigramCountList = new ArrayList<>(fm.getCollection().get(i).unigramCount.values());
				int max = Collections.max(unigramCountList);
				
				float sum = 0.0f; 
				Iterator it = fm.getCollection().get(i).unigramCount.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<String, Integer> pair = (Entry<String, Integer>) it.next();
					if (weight.containsKey(pair.getKey())) {
						// Normalize frequency and compute weight
						sum+=weight.get(pair.getKey())*((float)pair.getValue()/max);
					}
				}
				
				float network = 0.0f;
				if (sum>threshold) {
					network = 1.0f;
				}
				
				float desired_output = 0.0f;
				if (fm.getCollection().get(i).category) {
					desired_output = 1.0f; // positive
				}
				
				// defines +ve or -ve
				err =  desired_output - network;
				
				float correction = alpha*err;
				//Compute the final weights
				
				it = fm.getCollection().get(i).unigramCount.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<String, Integer> pair = (Entry<String, Integer>) it.next();
					if (weight.containsKey(pair.getKey())) {
						// Normalize frequency and compute weight
						weight.put(pair.getKey(),(weight.get(pair.getKey())+correction*((float)pair.getValue()/max)));
					}
				}
			}
		}
	}
}
