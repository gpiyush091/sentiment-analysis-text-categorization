package classifier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import dataStructure.Node;
import dataStructure.PreNode;
import fileHandler.FileManager;
import fileHandler.FileManagerSVM;

public class LanguageModelClassifier {
	
	ArrayList<PreNode> posData = new ArrayList<>();
	ArrayList<PreNode> negData = new ArrayList<>();
	
	class resNode {
		String classifierResult;
		String actualResult;
	}
	
	HashMap<String, resNode> resultSet = new HashMap<>();
	public ArrayList<Float> allAccur = new ArrayList<>();
	
	/**
	 * Returns argmax of an array of floats,
	 * argmax being the index of the maximum value.
	 * @param arr An array of float values
	 * @return index of maximum value
	 */
	private static Integer argmax(Float[] arr) {
		Float max = arr[0];
		Integer argmax = 0;
	    for (int i=1; i<arr.length; i++) {
	        if (arr[i] > max) {
	            max = arr[i];
	            argmax = i;
	        }
	    }
	    return argmax;
	}
	
	public static void main(String[] args) {
		System.out.println("Game begins");
		String dir = "F:\\remote\\Spring'14\\NLP\\HW1\\repo\\";
		LanguageModelClassifier lmd = new LanguageModelClassifier();
		lmd.segragrateData(dir+"train\\train1\\pos", dir+"train\\train1\\neg", dir+"test\\test1\\pos", dir+"test\\test1\\neg");
		lmd.segragrateData(dir+"train\\train2\\pos", dir+"train\\train2\\neg", dir+"test\\test2\\pos", dir+"test\\test2\\neg");
		lmd.segragrateData(dir+"train\\train3\\pos", dir+"train\\train3\\neg", dir+"test\\test3\\pos", dir+"test\\test3\\neg");
		lmd.segragrateData(dir+"train\\train4\\pos", dir+"train\\train4\\neg", dir+"test\\test4\\pos", dir+"test\\test4\\neg");
		lmd.segragrateData(dir+"train\\train5\\pos", dir+"train\\train5\\neg", dir+"test\\test5\\pos", dir+"test\\test5\\neg");
		lmd.accuracyLevels();
		System.out.println("Game ends");
	}
	
	public void accuracyLevels() {
		float sum = 0.0f;
		for (int i=0;i<allAccur.size();i++) {
			System.out.println("iteration: "+i+" accuracy: "+allAccur.get(i));
			sum += allAccur.get(i);
		}
		System.out.println("Avg accuracy: "+sum/5);
	}
	
	public void segragrateData(String trainPos, String trainNeg, String testPos, String testNeg) {
		FileManagerSVM fmPos = new FileManagerSVM();
		FileManagerSVM fmNeg = new FileManagerSVM();
		
		fmPos.readAllData(trainPos, true);
		fmNeg.readAllData(trainNeg, false);
		
		// System.out.println("pos corpus:"+fmPos.getCorpusSize());
		// System.out.println("neg corpus:"+fmNeg.getCorpusSize());
		
		PreprocessData pd = new PreprocessData();
		pd.processDataCollection(fmPos, fmNeg);
		
		FileManagerSVM testSet1 = new FileManagerSVM(); // pos
		testSet1.readAllData(testPos, null);
		test(testSet1.getCollection(), pd, true);
		
		FileManagerSVM testSet2 = new FileManagerSVM(); // neg
		testSet2.readAllData(testNeg, null);
		test(testSet2.getCollection(), pd, false);
		
		// displayHashMap();
		computeAccuracy();
		resultSet.clear();
	}
	
	private void displayHashMap() {
		Iterator it = resultSet.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, resNode> pair = (Map.Entry<String, resNode>) it.next();
			System.out.println("doc: "+pair.getKey()+" class: "+pair.getValue().classifierResult+" act: "+pair.getValue().actualResult);
		}
	}
	
	private void test(ArrayList<PreNode> docCollection, PreprocessData pd, Boolean category) {
		float probPosCategory = (float) Math.log(pd.getCategoryProbability(true));
		float probNegCategory = (float) Math.log(pd.getCategoryProbability(false));
		for (int i=0;i<docCollection.size();i++) {
			Iterator it = docCollection.get(i).bigramCount.entrySet().iterator();
			Float probPosProduct = 0.0f;
			Float probNegProduct = 0.0f;
			while (it.hasNext()) {
				Map.Entry<String, Integer> pair = (Map.Entry<String, Integer>) it.next();
				if (pd.bigramProbabilities.containsKey(pair.getKey())) {
					probPosProduct += (float)Math.log((float)(pd.bigramProbabilities.get(pair.getKey()).pos));
					probNegProduct += (float)Math.log((float)(pd.bigramProbabilities.get(pair.getKey()).neg));
				} else {
					probPosProduct += (float)Math.log((float)(1.0f/(pd.netBigramCount+pd.posBigramCount)));
					probNegProduct += (float)Math.log((float)(1.0f/(pd.netBigramCount+pd.negBigramCount)));
				}
			}
			float posProbDoc = Math.abs(probPosCategory + probPosProduct);
			float negProbDoc = Math.abs(probNegCategory + probNegProduct);
			// System.out.println("doc: "+docCollection.get(i).filename+" pos:"+posProbDoc+" neg:"+negProbDoc);
			resNode node = new resNode();
			if (category) {
				node.actualResult = "positive";
			} else {
				node.actualResult = "negative";
			}
			if (posProbDoc < negProbDoc) {
				node.classifierResult = "positive";
				resultSet.put(docCollection.get(i).filename, node);
			} else {
				node.classifierResult = "negative";
				resultSet.put(docCollection.get(i).filename, node);
			}
		}
	}
	
	private void computeAccuracy() {
		int accCount = 0;
		int netCount = resultSet.size();
		Iterator it = resultSet.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, resNode> pair = (Map.Entry<String, resNode>) it.next();
			if (pair.getValue().classifierResult.equals(pair.getValue().actualResult)) {
				accCount++;
			} else {
				// System.out.println("Incorrect ones:"+pair.getKey());
			}
		}
		System.out.println("accCount: "+accCount+" netCount: "+netCount);
		float percentage = ((float)accCount/netCount)*100;
		// System.out.println("Accuracy: "+ percentage);
		allAccur.add(percentage);
	}
}
