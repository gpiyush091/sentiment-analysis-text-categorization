package classifier;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import classifier.LanguageModelClassifier.resNode;
import dataStructure.Node;
import fileHandler.FileManager;
import fileHandler.FileManagerSVM;

public class PreprocessData {
	
	HashMap<String, Integer> unigramCollection = new HashMap<>();
	HashMap<String, Node> bigramCollection = new HashMap<>();
	
	public HashMap<String, Node> bigramProbabilities = new HashMap<>(); 
	
	int posCorpusCount = 0;
	int negCorpusCount = 0;
	int N = 0;
	
	public int posBigramCount = 0;
	public int negBigramCount = 0;
	public int netBigramCount = 0;
	
	FileManager fmPos = new FileManager();
	FileManager fmNeg = new FileManager();
	
	FileManagerSVM fmPosSVM = new FileManagerSVM();
	FileManagerSVM fmNegSVM = new FileManagerSVM();
	
	public void processDataCollection(FileManager fmPos, FileManager fmNeg) 
	{
		this.fmPos = fmPos;
		this.fmNeg = fmNeg;
		
		this.posCorpusCount = fmPos.getCorpusSize();
		this.negCorpusCount = fmNeg.getCorpusSize();
		this.N = posCorpusCount + negCorpusCount;
		
		createUnigramsCollection(fmPos, true);
		createUnigramsCollection(fmNeg, false);
		
		createBigramsCollection(fmPos, true);
		createBigramsCollection(fmNeg, false);
		
		populateBigramCount();
		setBigramProbabilities();
		
		// displayBigramProb();
	}
	
	public void processDataCollection(FileManagerSVM fmPos, FileManagerSVM fmNeg) 
	{
		this.fmPosSVM = fmPos;
		this.fmNegSVM = fmNeg;
		
		this.posCorpusCount = fmPos.getCorpusSize();
		this.negCorpusCount = fmNeg.getCorpusSize();
		this.N = posCorpusCount + negCorpusCount;
		
		createUnigramsCollection(fmPos, true);
		createUnigramsCollection(fmNeg, false);
		
		createBigramsCollection(fmPos, true);
		createBigramsCollection(fmNeg, false);
		
		populateBigramCount();
		setBigramProbabilities();
		
		// displayBigramProb();
	}
	
	private void displayBigramProb() {
		Iterator it = bigramProbabilities.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Node> pair = (Map.Entry<String, Node>) it.next();
			System.out.println("bigram: "+pair.getKey()+" pos: "+pair.getValue().pos+" neg: "+pair.getValue().neg);
		}
	}
	
	private void createUnigramsCollection(FileManagerSVM fm, Boolean category) 
	{
		/*
		 * iterate over each document and each word in hashmap and compute probabilites for each word (unigram) 
		 * */
		for (int i=0;i<fm.getCollection().size();i++) {
			// Unigram collection - contains the net count of each word in the whole corpus.
			Iterator it = fm.getCollection().get(i).unigramCount.entrySet().iterator();
			while(it.hasNext()) {
				Map.Entry<String, Integer> pair = (Entry<String, Integer>) it.next();
				if (!unigramCollection.containsKey(pair.getKey())) {
					unigramCollection.put(pair.getKey(), pair.getValue());
				} else {
					unigramCollection.put(pair.getKey(), unigramCollection.get(pair.getKey())+pair.getValue());
				}
			}
		}
	}
	
	private void createBigramsCollection(FileManagerSVM fm, Boolean category) 
	{
		for (int i=0; i<fm.getCollection().size(); i++) {
			Iterator it = fm.getCollection().get(i).bigramCount.entrySet().iterator();
			while(it.hasNext()) {
				Map.Entry<String, Integer> pair = (Entry<String, Integer>) it.next();
				if (!bigramCollection.containsKey(pair.getKey())) {
					Node newNode = new Node();
					if (category) {
						newNode.pos++;
					} else {
						newNode.neg++;
					}
					bigramCollection.put(pair.getKey(), newNode);
				} else {
					Node oldNode = bigramCollection.get(pair.getKey()); 
					if (category) {
						oldNode.pos++;
					} else {
						oldNode.neg++;
					}
					bigramCollection.put(pair.getKey(), oldNode);
				}
			}
		}
	}
	
	private void createUnigramsCollection(FileManager fm, Boolean category) 
	{
		/*
		 * iterate over each document and each word in hashmap and compute probabilites for each word (unigram) 
		 * */
		for (int i=0;i<fm.getCollection().size();i++) {
			// Unigram collection - contains the net count of each word in the whole corpus.
			Iterator it = fm.getCollection().get(i).unigramCount.entrySet().iterator();
			while(it.hasNext()) {
				Map.Entry<String, Integer> pair = (Entry<String, Integer>) it.next();
				if (!unigramCollection.containsKey(pair.getKey())) {
					unigramCollection.put(pair.getKey(), pair.getValue());
				} else {
					unigramCollection.put(pair.getKey(), unigramCollection.get(pair.getKey())+pair.getValue());
				}
			}
		}
	}
	
	private void createBigramsCollection(FileManager fm, Boolean category) 
	{
		for (int i=0; i<fm.getCollection().size(); i++) {
			Iterator it = fm.getCollection().get(i).bigramCount.entrySet().iterator();
			while(it.hasNext()) {
				Map.Entry<String, Integer> pair = (Entry<String, Integer>) it.next();
				if (!bigramCollection.containsKey(pair.getKey())) {
					Node newNode = new Node();
					if (category) {
						newNode.pos++;
					} else {
						newNode.neg++;
					}
					bigramCollection.put(pair.getKey(), newNode);
				} else {
					Node oldNode = bigramCollection.get(pair.getKey()); 
					if (category) {
						oldNode.pos++;
					} else {
						oldNode.neg++;
					}
					bigramCollection.put(pair.getKey(), oldNode);
				}
			}
		}
	}
	
	public float getCategoryProbability(Boolean category) {
		if (category) {
			float pos = (float)posCorpusCount/N;
			return pos;
		} else {
			float neg = (float)negCorpusCount/N;
			return neg;
		}
	}
	
	private void populateBigramCount() {
		Iterator it = bigramCollection.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry<String, Node> pair = (Entry<String, Node>) it.next();
			posBigramCount += pair.getValue().pos;
			negBigramCount += pair.getValue().neg;
		}
		netBigramCount = posBigramCount + negBigramCount;
	}
	
	public void setBigramProbabilities() {
		Iterator it = bigramCollection.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Node> pair = (Entry<String, Node>) it.next();
			Node newNode = new Node();
			newNode.pos = getBigramInCategoryProbability(pair.getKey(), true);
			newNode.neg = getBigramInCategoryProbability(pair.getKey(), false);
			bigramProbabilities.put(pair.getKey(), newNode);
		}
	}
	
	/*
	 * Compute P(x_i|c_j) = (N(X=x_i,C=c_j)+1)/(N(C=c_j)+k)
	 * k = vocab length or # of bigrams
	 * */
	private float getBigramInCategoryProbability(String bigram, Boolean category) {
		if (category) {
			return (((getBigramCountInCategory(bigram, category)+1)/(posBigramCount+netBigramCount))*1000000);
		} else {
			return (((getBigramCountInCategory(bigram, category)+1)/(negBigramCount+netBigramCount))*1000000);
		}
	}
	
	private Float getBigramCountInCategory(String bigram, Boolean category) {
		if (bigramCollection.containsKey(bigram)) {
			if (category) {
				return bigramCollection.get(bigram).pos;
			} else {
				return bigramCollection.get(bigram).neg;
			}
		}
		return 0.0f;
	}
}
