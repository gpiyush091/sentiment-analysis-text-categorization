package classifier;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

import dataStructure.Node;
import fileHandler.FileManager;

public class SVMClassifier {
	HashMap<String, Integer> unigramCollection = new HashMap<>();
	HashMap<String, Integer> bigramCollection = new HashMap<>();
	int unigramID = 0;
	int bigramID = 0;
	String dir = "F:\\remote\\Spring'14\\NLP\\HW1\\repo\\";
	ArrayList<String> featureSets = new ArrayList<>();
	
	public static void main(String[] args) {
		System.out.println("Game begins");
		SVMClassifier svm = new SVMClassifier();
		svm.segragrateData(svm.dir+"train\\train1\\pos", svm.dir+"train\\train1\\neg", svm.dir+"test\\test1\\pos", svm.dir+"test\\test1\\neg");
		// svm.segragrateData(svm.dir+"train\\train2\\pos", svm.dir+"train\\train2\\neg", svm.dir+"test\\test2\\pos", svm.dir+"test\\test2\\neg");
		// svm.segragrateData(svm.dir+"train\\train3\\pos", svm.dir+"train\\train3\\neg", svm.dir+"test\\test3\\pos", svm.dir+"test\\test3\\neg");
		// svm.segragrateData(svm.dir+"train\\train4\\pos", svm.dir+"train\\train4\\neg", svm.dir+"test\\test4\\pos", svm.dir+"test\\test4\\neg");
		// svm.segragrateData(svm.dir+"train\\train5\\pos", svm.dir+"train\\train5\\neg", svm.dir+"test\\test5\\pos", svm.dir+"test\\test5\\neg");
		// svm.accuracyLevels();
		System.out.println("Game ends");
	}

	int setTrainNumber = 1;
	int setTestNumber = 1;
	public void segragrateData(String trainPos, String trainNeg, String testPos, String testNeg) {
		unigramCollection.clear();
		bigramCollection.clear();
		featureSets.clear();
		unigramID = 0;
		bigramID = 0;
		FileManager fmPos = new FileManager();
		FileManager fmNeg = new FileManager();

		fmPos.readAllData(trainPos, true);
		fmNeg.readAllData(trainNeg, false);
		
		createUnigramsCollection(fmPos, true);
		createUnigramsCollection(fmNeg, false);

		createBigramsCollection(fmPos, true);
		createBigramsCollection(fmNeg, false);
		
		System.out.println("pos corpus:"+fmPos.getCorpusSize());
		System.out.println("neg corpus:"+fmNeg.getCorpusSize());

		createTrainDataSet(fmPos, true, true); // unigram
		createTrainDataSet(fmNeg, false, true); // unigram
		
		createTrainDataSet(fmPos, true, false); // bigram
		createTrainDataSet(fmNeg, false, false); // bigram

		String filename = dir+"svm_train_sets\\"+setTrainNumber+".dat";
		System.out.println("Train"+setTrainNumber+".dat");
		dumpDataInFile(filename);
		setTrainNumber++;
		
		featureSets.clear();
		
		FileManager testSet1 = new FileManager();
		testSet1.readAllData(testPos, null);

		FileManager testSet2 = new FileManager();
		testSet2.readAllData(testPos, null);

		createTrainDataSet(testSet1, true, true); // unigram
		createTrainDataSet(testSet2, false, true); // unigram
		
		createTrainDataSet(testSet1, true, false); // bigram
		createTrainDataSet(testSet2, false, false); // bigram

		filename = dir+"svm_test_sets\\"+setTestNumber+".dat";
		System.out.println("Test"+setTestNumber+".dat");
		dumpDataInFile(filename);
		setTestNumber++;
		featureSets.clear();
	}

	private void dumpDataInFile(String filename) {
		File newFile = new File(filename);

		if (!newFile.exists()) {
			try {
				newFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newFile), "UTF-8"));
			for (int i=0;i<featureSets.size();i++) {
				out.append(featureSets.get(i));
				out.append("\n");
			}
			out.flush();
			out.close();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// We need to create a unigram collection for ids if features have to be <qid>

	private void createUnigramsCollection(FileManager fm, Boolean category) 
	{
		/*
		 * iterate over each document and each word in hashmap and compute probabilites for each word (unigram) 
		 * */
		for (int i=0;i<fm.getCollection().size();i++) {
			// Unigram collection - contains the net count of each word in the whole corpus.
			Iterator it = fm.getCollection().get(i).unigramCount.entrySet().iterator();
			while(it.hasNext()) {
				Map.Entry<String, Integer> pair = (Entry<String, Integer>) it.next();
				if (pair.getValue()>=4) {
					unigramID++;
					if (!unigramCollection.containsKey(pair.getKey())) {
						unigramCollection.put(pair.getKey(), unigramID);
					}
				}
			}
		}
	}
	
	private void createBigramsCollection(FileManager fm, Boolean category) 
	{
		/*
		 * iterate over each document and each word in hashmap and compute probabilites for each word (unigram) 
		 * */
		for (int i=0;i<fm.getCollection().size();i++) {
			// Unigram collection - contains the net count of each word in the whole corpus.
			Iterator it = fm.getCollection().get(i).bigramCount.entrySet().iterator();
			while(it.hasNext()) {
				Map.Entry<String, Integer> pair = (Entry<String, Integer>) it.next();
				if (pair.getValue()>=4) {
					bigramID++;
					if (!bigramCollection.containsKey(pair.getKey())) {
						bigramCollection.put(pair.getKey(), bigramID);
					}
				}
			}
		}
	}

	private void createTrainDataSet(FileManager fm, Boolean category, Boolean ngramType) {
		if (ngramType) {
			for (int i=0;i<fm.getCollection().size(); i++) {
				String featureList = "";
								
				HashMap<String, Float> tfidf = new HashMap<>();
				Iterator itx = fm.getCollection().get(i).unigramCount.entrySet().iterator();
				List<Integer> mapValues = new ArrayList<>(fm.getCollection().get(i).unigramCount.values());
				int max = Collections.max(mapValues);
				while (itx.hasNext()) {
					Map.Entry<String, Integer> pair = (Entry<String, Integer>) itx.next();
					float tf = (float) (0.5+((0.5*((float)pair.getValue()))/(max)));
					float idf = (float)Math.log((float)fm.getCollection().size()/getTermPresenceInDocumentCount(pair.getKey(), fm));
					float tfidfVal = tf*idf;
					tfidf.put(pair.getKey(), tfidfVal);
				}
				
				if (category) {
					featureList = featureList+"+1"+" ";
				} else {
					featureList = featureList+"-1"+" ";
				}

				TreeMap<Integer, Float> sortedMap = new TreeMap<>();

				Iterator it = tfidf.entrySet().iterator();
				//Iterator it = fm.getCollection().get(i).unigramCount.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<String, Float> pair = (Entry<String, Float>) it.next();
					if (unigramCollection.containsKey(pair.getKey())) {
						//Object val = pair.getValue();
						//Float comp2 = new Float((int)val);
						//sortedMap.put(unigramCollection.get(pair.getKey()), comp2);
						sortedMap.put(unigramCollection.get(pair.getKey()), pair.getValue());
					}
				}
				
				
				if (sortedMap.size()>0) {
					List<Float> mapValues1 = new ArrayList<>(sortedMap.values());
					float max1 = Collections.max(mapValues1);
					it = sortedMap.entrySet().iterator();
					while (it.hasNext()) {
						Map.Entry<Integer, Float> pair = (Entry<Integer, Float>) it.next();
						float val = (float)pair.getValue()/max1; 
						featureList = featureList+pair.getKey()+":"+val+" ";
					}
					featureSets.add(featureList);
				}
			}
		} else {
			for (int i=0;i<fm.getCollection().size(); i++) {
				String featureList = "";

				HashMap<String, Float> tfidf = new HashMap<>();
				Iterator itx = fm.getCollection().get(i).bigramCount.entrySet().iterator();
				List<Integer> mapValues = new ArrayList<>(fm.getCollection().get(i).bigramCount.values());
				int max = Collections.max(mapValues);
				while (itx.hasNext()) {
					Map.Entry<String, Integer> pair = (Entry<String, Integer>) itx.next();
					float tf = (float) (0.5+((0.5*((float)pair.getValue()))/(max)));
					float idf = (float)Math.log((float)fm.getCollection().size()/getTermPresenceInDocumentCount(pair.getKey(), fm));
					float tfidfVal = tf*idf;
					tfidf.put(pair.getKey(), tfidfVal);
				}
				
				if (category) {
					featureList = featureList+"+1"+" ";
				} else {
					featureList = featureList+"-1"+" ";
				}

				TreeMap<Integer, Float> sortedMap = new TreeMap<>();

				Iterator it = tfidf.entrySet().iterator();
				int index = 1;
				while (it.hasNext()) {
					Map.Entry<String, Float> pair = (Entry<String, Float>) it.next();

					if (bigramCollection.containsKey(pair.getKey())) {
						//Object val = pair.getValue();
						//Float comp2 = new Float((int)val);
						//sortedMap.put(unigramCollection.get(pair.getKey()), comp2);
						System.out.println(pair.getValue());
						sortedMap.put(bigramCollection.get(pair.getKey()), pair.getValue());
						index++;
					}
				}
				
				if (sortedMap.size()>0) {
					List<Float> mapValues1 = new ArrayList<>(sortedMap.values());
					float max1 = Collections.max(mapValues1);
					it = sortedMap.entrySet().iterator();
					while (it.hasNext()) {
						Map.Entry<Integer, Float> pair = (Entry<Integer, Float>) it.next();
						float val = (float)pair.getValue()/max1;
						featureList = featureList+pair.getKey()+":"+val+" ";
					}
					featureSets.add(featureList);
				}
			}
		}
	}
	
	private float getTermPresenceInDocumentCount(String str, FileManager fm) {
		float count = 0;
		for (int i=0;i<fm.getCollection().size();i++) {
			if (fm.getCollection().get(i).unigramCount.containsKey(str)) {
				count++;
			}
		}
		return count;
	}
	
	private LinkedHashMap<String, Float> sortHashMapByValuesD(HashMap<String, Integer> passedMap) {
		List<Integer> mapValues = new ArrayList<Integer>(passedMap.values());
		Collections.sort(mapValues);

		LinkedHashMap<String, Float> sortedMap = new LinkedHashMap<String, Float>();

		Iterator valueIt = mapValues.iterator();
		while (valueIt.hasNext()) {
			Object val = valueIt.next();
			Iterator keyIt = passedMap.entrySet().iterator();

			while (keyIt.hasNext()) {
				Map.Entry<String, Integer> pair = (Entry<String, Integer>) keyIt.next();
				String key = pair.getKey();
				Float comp1 = new Float(passedMap.get(key));
				Float comp2 = new Float((int)val);
				if (comp1.equals(comp2)){
					passedMap.remove(key);
					sortedMap.put((String) key, comp2);
					break;
				}
			}
		}
		return sortedMap;
	}

	private void displayHashMap(HashMap hm) {
		Iterator it = hm.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Node> pair = (Map.Entry<String, Node>) it.next();
			System.out.println("Key: "+pair.getKey()+" Value: "+pair.getValue());
		}
	}
}
